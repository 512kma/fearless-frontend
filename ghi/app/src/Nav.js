
function Nav() {
  return (
    <div className="navbar navbar-expand-lg navbar-light bg-light">
      <ul className="navbar-nav me-auto mb-2 mb-lg-0">
        <li> <a className="navbar-brand" href="index.html">Conference GO!</a></li>
        <li className="nav-item">
          <a className="nav-link active" href="http://localhost:3000/index.html">Home</a>
        </li>
        <li className="nav-item">
          <a className="nav-link active" href="http://localhost:3000/new-location.html">New Location</a>
        </li>
        <li className="nav-item">
          <a className="nav-link active" href="http://localhost:3000/new-conference.html">New Conference</a>
        </li>
        <li className="nav-item">
          <a className="nav-link active" href="http://localhost:3000/new-presentation.html">New Presentation</a>
        </li>
      </ul>
    </div>
  );
}

export default Nav;
