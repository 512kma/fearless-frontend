function createCard(name, description, pictureUrl, locationName, formattedDate) {
  return `
  <div class='col'>
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title" style="">${name}</h5>
          <h6 class="card-city" style="font-style:italic ; color: grey">${locationName}</h6>
          <p class="card-text">${description}</p>
          <p class="card-time">${formattedDate}</p>
        </div>
      </div>
    </div>
  `;
}


window.addEventListener("DOMContentLoaded", async () => {

  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.log("BAD REQUEST LOSER");
      const container = document.querySelector('.container');
      container.innerHTML += `<div>~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~YOU MESSED UP, TRY AGAIN TOMORROW.</div>`;

    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailURL = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailURL);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const locationName = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const startDate = new Date(starts).toLocaleDateString();
          const endDate = new Date(ends).toLocaleDateString();
          const formattedDate = startDate + '-' + endDate;
          const html = createCard(
            title,
            description,
            pictureUrl,
            locationName,
            formattedDate,
            startDate,
            endDate
          );
          const column = document.querySelectorAll('.row');
          column.forEach((column) => {
            column.innerHTML += html;
          });
        }
      }
    }
  } catch (e) {
    console.log("an error occured - you silly, silly goose.");
  }
});
